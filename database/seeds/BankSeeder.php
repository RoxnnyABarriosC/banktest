<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            'name' => 'Banco de Venezuela',
            'code' => '0102',
            'updated_at' => now(),
            'created_at' => now()
        ]);
        DB::table('banks')->insert([
            'name' => 'Banco Mercantil',
            'code' => '0105',
            'updated_at' => now(),
            'created_at' => now()
        ]);
        DB::table('banks')->insert([
            'name' => 'Banco de Bicentenario',
            'code' => '0175',
            'updated_at' => now(),
            'created_at' => now()
        ]);
        DB::table('banks')->insert([
            'name' => 'Banesco banco universal',
            'code' => '0134',
            'updated_at' => now(),
            'created_at' => now()
        ]);
    }
}
