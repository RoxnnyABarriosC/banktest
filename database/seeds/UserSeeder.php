<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role' => 'admin',
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin'),
            'remember_token' => Str::random(10),
            'updated_at' => now(),
            'created_at' => now()
        ]);
        DB::table('users')->insert([
            'role' => 'client',
            'name' => 'client',
            'email' => 'client@client.com',
            'email_verified_at' => now(),
            'password' => Hash::make('client'),
            'remember_token' => Str::random(10),
            'updated_at' => now(),
            'created_at' => now()
        ]);
    }
}
