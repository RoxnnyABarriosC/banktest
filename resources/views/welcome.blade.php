<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Socket.IO</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

         <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="container" id="app">
            <h1 class="text-muted">Laravel Broadcast Redis Socket.IO</h1>
            <div id="notification"></div>
        </div>
    </body>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>

    <script>
        const userId = '{{ auth()->id() }}';

         window.Echo.channel('msg')
        .listen('.MsgServerTest',(data) => {
            console.log(data.message);
            $('#notification').append('<div class="alert alert-warning">'+data.message+'</div>');
        });

        window.Echo.private('User.'+userId)
        .listen('.Auth',(data) => {
            console.log(data.message);
            $('#notification').append('<div class="alert alert-danger">'+data.message+'</div>');
        });

    </script>
</html>

