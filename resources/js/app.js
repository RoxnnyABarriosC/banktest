import "./bootstrap";

import Vue from "vue";

import router from "./app/router";
import store from "./app/store";

import App from "./app/app.vue";

window.store = store;

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
