import Vue from "vue";
import Vuex, { Store } from "vuex";

// * MODULES
import auth from "./modules/auth";
import app from "./modules/app";
import client from "./modules/client";
import bank from "./modules/bank";
import notification from "./modules/notification";

Vue.use(Vuex);

export default new Store({
    modules: {
        auth,
        app,
        client,
        bank,
        notification
    }
});
