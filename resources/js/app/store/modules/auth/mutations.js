export function SET_USER(state, user) {
    state.user = user;
    state.auth = Boolean(user);
    if (state.auth) state.admin = Boolean(user.role === "admin");
    else state.admin = false;
    store.commit("app/SET_INIT", { root: true });
}
