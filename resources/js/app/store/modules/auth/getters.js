export function name(state) {
    return state.user?.name;
}

export function email(state) {
    return state.user?.email;
}

export function role(state) {
    return state.user?.role;
}

export function auth(state) {
    return state.auth;
}

export function admin(state) {
    return state.admin;
}
