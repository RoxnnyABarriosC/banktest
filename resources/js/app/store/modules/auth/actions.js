export async function me({ commit }) {
    await axios
        .get("/api/auth/me")
        .then(res => {
            commit("auth/SET_USER", res.data, { root: true });
        })
        .catch(() => {
            commit("auth/SET_USER", null, { root: true });
        });
}

export async function login({ commit }, credentials) {
    let errors = false;
    await axios.get("/sanctum/csrf-cookie");
    await axios
        .post("/login", credentials)
        .then(res => {
            commit("auth/SET_USER", res.data, { root: true });
        })
        .catch(err => {
            commit("auth/SET_USER", null, { root: true });
            if (err.response.status == 422) errors = err.response.data.errors;
        });
    return errors;
}

export async function signup({ commit }, credentials) {
    let errors = false;
    await axios
        .post("/signup", credentials)
        .then(res => {
            commit("auth/SET_USER", res.data, { root: true });
        })
        .catch(err => {
            commit("auth/SET_USER", null, { root: true });
            if (err.response.status == 422) errors = err.response.data.errors;
        });
    return errors;
}

export async function logout({ dispatch, commit }) {
    await axios.post("/logout").then(() => {
        commit("auth/SET_USER", null, { root: true });
    });
}

export async function update({ commit }, data) {
    let errors = false;
    await axios
        .put("/api/auth/update", data)
        .then(res => {
            commit("auth/SET_USER", res.data, { root: true });
        })
        .catch(err => {
            if (err.response.status == 422) errors = err.response.data.errors;
        });
    return errors;
}

export async function changePassword({ commit }, credentials) {
    let errors = false;
    await axios
        .put("/api/auth/changepassword", credentials)
        .then(() => {})
        .catch(err => {
            if (err.response.status == 422) errors = err.response.data.errors;
        });
    return errors;
}
