export async function getClients({ commit }) {
    await axios
        .get("/api/clients/")
        .then(res => {
            commit("SET_CLIENTS", res.data);
        })
        .catch(() => {
            commit("SET_CLIENTS", null);
        });
}
