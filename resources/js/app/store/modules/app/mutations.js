export function SET_INIT(state) {
    state.init = true;
}

export function SET_USER_UPDATE(state, value) {
    state.userUpdate = value;
}
