export async function getBanks({ commit }) {
    await axios
        .get("/api/banks/")
        .then(res => {
            commit("SET_BANKS", res.data);
        })
        .catch(() => {
            commit("SET_BANKS", null);
        });
}

export async function storeBank({ commit }, bank) {
    let errors = false;
    console.log(bank);
    await axios
        .post("/api/banks/", bank)
        .then(res => {
            commit("PUSH_BANK", res.data);
        })
        .catch(err => {
            if (err.response.status == 422) errors = err.response.data.errors;
        });
    return errors;
}

export async function deleteBank({ commit }, id) {
    await axios
        .delete(`/api/banks/${id}`)
        .then(res => {
            commit("SET_BANKS", res.data);
        })
        .catch(() => {
            commit("SET_BANKS", null);
        });
}
