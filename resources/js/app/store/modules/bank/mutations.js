export function SET_BANKS(state, banks) {
    state.banks = banks;
}
export function PUSH_BANK(state, bank) {
    state.banks.push(bank);
}
