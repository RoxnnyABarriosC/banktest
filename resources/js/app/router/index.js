import Vue from "vue";
import VueRouter from "vue-router";

// * PAGES
import Home from "./../pages/Home";
import NotFound from "./../pages/404";
import Login from "./../pages/Login";
import Signup from "./../pages/Signup";

// * MIDDLEWARES
import auth from "./middlewares/auth";
import notAuth from "./middlewares/notAuth";

// * NEXT FACTORY
import nextFactory from "./nextFactory";

// * Vue use router
Vue.use(VueRouter);

// * Routes
const routes = [
    {
        path: "*",
        redirect: "/404"
    },
    {
        path: "/",
        alias: "/home",
        name: "home",
        component: Home,
        meta: {
            middleware: [auth]
        }
    },
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            middleware: [notAuth]
        }
    },
    {
        path: "/signup",
        name: "signup",
        component: Signup,
        meta: {
            middleware: [notAuth]
        }
    },
    {
        path: "/404",
        name: "404",
        component: NotFound
    }
];

const router = new VueRouter({
    mode: "history",
    routes
});

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware)
            ? to.meta.middleware
            : [to.meta.middleware];

        const context = {
            from,
            next,
            router,
            to
        };
        const nextMiddleware = nextFactory(context, middleware, 1);

        return middleware[0]({ ...context, next: nextMiddleware });
    }

    return next();
});

export default router;
