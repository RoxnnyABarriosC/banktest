export default async function authMiddleware({ next, router }) {
    if (!store.getters["app/init"]) {
        await store.dispatch("auth/me");
        if (!store.getters["auth/auth"]) {
            return router.push("/login");
        }
    }
    return next();
}
