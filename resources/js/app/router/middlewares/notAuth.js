export default async function notAuthMiddleware({ next, router, to }) {
    if (!store.getters["app/init"]) {
        await store.dispatch("auth/me");
        if (store.getters["auth/auth"]) {
            return router.push("/");
        }
    }
    return next();
}
