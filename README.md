## WebSocket Auth

Instrucciones para levantar el servidor y testear la app

**Configurando las variables de entorno**

```sh
# configuracion para la base de datos
DB_CONNECTION= pgsql | mysql
DB_HOST= 127.0.0.1
DB_PORT= 5432 | 3306
DB_DATABASE= # Nombre de la base de datos
DB_USERNAME= # Usuario del gestor
DB_PASSWORD= # Clave
```

```sh
# configuracion para redis
BROADCAST_DRIVER=redis
QUEUE_CONNECTION=redis
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
REDIS_CLIENT=predis
REDIS_CLUSTER=redis
```

**Ejecute los sigientes comandos en la carpeta raiz del proyecto**

```sh
$ composer install
```

```sh
$ npm i -D
```

```sh
$ npm i -g laravel-echo-server
```

```sh
$ php artisan migrate:fresh --seed
```

**NOTA:** A partir de aqui todos los comandos deben ejecutarse en terminales distintas

```sh
$ npm run watch
```

```sh
$ php artisan serve
```

**NOTA:** Es necesario tener **Redis** añadido al path de windows para ejecutar su comando

```sh
$  redis-server
```

-   En caso de no tener instalado **redis** en su pc, en la carpeta raiz del proyecto estan los archivos necesarios dentro de una careta llamada Redis.

    Se le recomienda copiar la carpeta y colocarla en cualquier lugar de su pc, preferiblemente en su disco duroprincipal donde esta instalado **wamp** o **xampp**, añadirla al path de Windows y luego ejecutar el comando antes mencionado.

    En dado caso de no realizar lo anterior, solo ejecute el archivo **redis-server.exe** que se encuentra dentro de la carpeta Redis en la raiz del proyecto.

```sh
$ php artisan queue:work
```

```sh
$  laravel-echo-server start
```

a partir de este momento ya puede probar la app accediento al http://localhost:8000/.

**Errores conocidos**

-   Los enventos privados no estan siendo capturados por **laravel-echo-server** por problemas de autenticacion.
