<?php

use App\User;
use App\Events\PublicMessage;
use App\Events\PrivateMessage;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{view}', 'SpaController@index')->where('view', '.*')->name('home');

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('signup', 'Auth\RegisterController@register');

// * TEST SOCKET
Route::post('/test', function () {
    event(new PublicMessage('This notification is a public message'));
    dd('Public event executed successfully');
});

Route::post('/private-test', function () {
    event(new PrivateMessage(\Auth::user()));
    dd('Private event executed successfully');
});
