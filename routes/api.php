<?php

use Illuminate\Http\Request;
use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['middleware' => 'auth:sanctum'])->group(function () {

    // * AUTH ROUTE
    Route::prefix('auth')->group(function () {
        Route::get('/me', 'AuthController@me');
        Route::put('/update', "AuthController@update");
        Route::put('/changepassword', "AuthController@changePassword");
    });

    Route::middleware([IsAdmin::class])->group(function () {
        // * CLIENTS ROUTES
        Route::prefix('clients')->group(function () {
            Route::get('/', 'ClientController@clients');
        });
        // * BANKS ROUTES
        Route::prefix('banks')->group(function () {
            Route::get('/', 'BankController@banks');
            Route::post('/', 'BankController@store');
            Route::delete('/{id}', 'BankController@destroy');
        });
    });
});
