<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{

    protected $fillable = ['name', 'code'];

    protected $casts = [
        'id' => 'Int',
        'code' => 'Int'
    ];
}
