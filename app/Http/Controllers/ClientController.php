<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function clients(Request $request)
    {
        return response()->json(User::where('role', 'client')->get());
    }
}
