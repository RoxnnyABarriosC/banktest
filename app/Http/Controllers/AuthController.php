<?php

namespace App\Http\Controllers;

use App\User;
use App\Rules\MePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function me(Request $request)
    {
        return response()->json(\Auth::user());
    }

    public function update(Request $request)
    {
        $id = Auth::id();

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id
        ]);

        $user = User::find($id);

        $user->name = request('name');
        $user->email = request('email');

        $user->update();

        return response()->json($user);
    }

    public function changePassword(Request $request)
    {
        $this->validate(request(), [
            'currentPassword' => ['required', 'string', new MePassword],
            'newPassword' => ['required', 'string', 'min:8', 'required_with:confirmPassword', 'same:confirmPassword'],
            'confirmPassword' => 'required|string',
        ]);
        $user = User::find(Auth::id());
        $user->password = Hash::make(request('newPassword'));
        $user->update();
        return response()->json($user);
    }
}
